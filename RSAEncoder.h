#pragma once
#include <string>
#include <vector>
#define CHARS_SIZE 5000

class RSAEncoder
{
public:
	RSAEncoder();
	~RSAEncoder();

	std::vector<std::string> RSA_Encode(const char *s, long e, long n);
	


private:
	char _chars[CHARS_SIZE];
	std::vector<std::string> _result;
};

