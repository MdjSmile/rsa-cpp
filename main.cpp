// RSA.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "RSADecoder.h"
#include "RSAEncoder.h"
#include <stdio.h>

int main()
{
	RSADecoder *decoder = new RSADecoder();
	RSAEncoder *encoder = new RSAEncoder();

	long e, n;
	decoder->getPublicKeys(&e, &n);

	std::vector<std::string> encodedStrings = encoder->RSA_Encode("1|Username|P@ssword1", e, n);

	printf("%s", decoder->RSA_Decode(encodedStrings));


	return 0;
}