#pragma once
#include <string>
#include <vector>
#define CHARS_SIZE 5000
class RSA
{
public:
	RSA();
	~RSA();

public: 
	
	
private:
	long _d, _n, _p, _q;
	char _chars[CHARS_SIZE];
	std::vector<std::string> _result;

	std::vector<std::string> RSA_Encode(const char *s);
	const char *RSA_Decode(std::vector<std::string> input);
	
};

bool isTheNumberSimple(long n);
long calculate_d(long m);
long calculate_e(long d, long m);
int indexOf(char *cA, char c);

