#pragma once
#include <string>
#include <vector>
#define CHARS_SIZE 5000

class RSADecoder
{
public:
	RSADecoder();
	~RSADecoder();

	void getPublicKeys(long *e, long *n);
	const char *RSA_Decode(std::vector<std::string> input);

private:
	long _d, _n, _p, _q;
	long _e;
	char _chars[CHARS_SIZE];
	std::vector<std::string> _result;
};

