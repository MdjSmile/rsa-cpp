#include "pch.h"
#include "RSA.h"
#include "BigInt.h"


#pragma warning(disable:4996)

RSA::RSA()
{
	_p = 13;
	_q = 17;
	strcpy(_chars, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+=-|");
}


RSA::~RSA()
{
	delete _chars;
}

std::vector<std::string> RSA::RSA_Encode(const char *s)
{
	std::vector<std::string> resultVector;
	if (isTheNumberSimple(_p) && isTheNumberSimple(_q))
	{
		_n = _p * _q;
		long m = (_p - 1) * (_q - 1);
		_d = calculate_d(m);
		long e_ = calculate_e(_d, m);

		size_t i = 0;
		bigint bi;
		while (s[i] != '\0' && i < CHARS_SIZE)
		{
			int index = indexOf(_chars, s[i]);
			bi = index;
			bi = bi ^ (int)e_;
			bigint n_ = _n;
			bi = bi % n_;
			resultVector.push_back(bi.to_string());
			i++;
		}
	}

	return resultVector;
}

const char *RSA::RSA_Decode(std::vector<std::string> input)		// server decode
{
	char result[CHARS_SIZE];
	strcpy(result, "");
	bigint bi;

	for (int i = 0; i < input.size(); i++)
	{
		bi = input.at(i);
		bi = bi ^ _d;
		bi = bi % _n;
		int index = std::atoi(bi.to_string().c_str());
		char s = _chars[index];
		result[i] = _chars[index];
		result[i + 1] = '\0';
	}


	return (const char*)result;
}

bool isTheNumberSimple(long n)
{
	if (n < 2)
		return false;

	if (n == 2)
		return true;

	for (long i = 2; i < n; i++)
		if (n % i == 0)
			return false;

	return true;
}

long calculate_d(long m)
{
	long d = m - 1;

	for (long i = 2; i <= m; i++)
		if ((m % i == 0) && (d % i == 0)) //если имеют общие делители
		{
			d--;
			i = 1;
		}

	return d;
}

long calculate_e(long d, long m)
{
	long e = 10;

	while (true)
	{
		if ((e * d) % m == 1)
			break;
		e++;
	}

	return e;
}

int indexOf(char *cA, char c)
{
	int i = 0;
	while (cA[i] != '\0' || i > CHARS_SIZE)
	{
		if (cA[i] == c) return i;
		i++;
	}
	return -1;
}
