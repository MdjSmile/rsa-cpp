#include "pch.h"
#include "RSADecoder.h"
#include "BigInt.h"
#include "RSA.h"
#pragma warning(disable:4996)

RSADecoder::RSADecoder()
{
	_p = 13;
	_q = 17;
	strcpy(_chars, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+=-|");
	if (isTheNumberSimple(_p) && isTheNumberSimple(_q))
	{
		_n = _p * _q;
		long m = (_p - 1) * (_q - 1);
		_d = calculate_d(m);
		_e = calculate_e(_d, m);
		
	}
}


RSADecoder::~RSADecoder()
{

}

void RSADecoder::getPublicKeys(long *e, long *n)
{
	*e = _e;
	*n = _n;
}

const char *RSADecoder::RSA_Decode(std::vector<std::string> input)
{
	char result[CHARS_SIZE];
	strcpy(result, "");
	bigint bi;

	for (int i = 0; i < input.size(); i++)
	{
		bi = input.at(i);
		bi = bi ^ _d;
		bi = bi % _n;
		int index = std::atoi(bi.to_string().c_str());
		char s = _chars[index];
		result[i] = _chars[index];
		result[i + 1] = '\0';
	}

	return (const char*)result;
}
