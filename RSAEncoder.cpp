#include "pch.h"
#include "RSAEncoder.h"
#include "BigInt.h"
#include "RSA.h"

#pragma warning(disable:4996)

RSAEncoder::RSAEncoder()
{
	strcpy(_chars, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+=-|");
}


RSAEncoder::~RSAEncoder()
{
}

std::vector<std::string> RSAEncoder::RSA_Encode(const char *s, long e, long n)
{
	std::vector<std::string> resultVector;
	
	size_t i = 0;
	bigint bi;
	while (s[i] != '\0' && i < CHARS_SIZE)
	{
		int index = indexOf(_chars, s[i]);
		bi = index;
		bi = bi ^ (int)e;
		bigint n_ = n;
		bi = bi % n_;
		resultVector.push_back(bi.to_string());
		i++;
	}

	return resultVector;
}
